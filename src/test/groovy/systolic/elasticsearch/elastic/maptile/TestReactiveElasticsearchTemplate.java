package systolic.elasticsearch.elastic.maptile;

import org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient;
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.convert.ElasticsearchConverter;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;

public class TestReactiveElasticsearchTemplate extends ReactiveElasticsearchTemplate {

    private String testIndexName;

    public TestReactiveElasticsearchTemplate(ReactiveElasticsearchClient client) {
        super(client);
    }

    public TestReactiveElasticsearchTemplate(ReactiveElasticsearchClient client, ElasticsearchConverter converter) {
        super(client, converter);
    }

    public String getTestIndexName() {
        return testIndexName;
    }

    public void setTestIndexName(String testIndexName) {
        this.testIndexName = testIndexName;
    }

    @Override
    public IndexCoordinates getIndexCoordinatesFor(Class<?> clazz) {
        if (testIndexName!=null) {
            return IndexCoordinates.of(testIndexName);
        } else {
            return super.getIndexCoordinatesFor(clazz);
        }
    }
}
