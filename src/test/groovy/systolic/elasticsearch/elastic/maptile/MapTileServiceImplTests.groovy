package systolic.elasticsearch.elastic.maptile

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import reactor.core.publisher.Flux
import reactor.test.StepVerifier
import reactor.util.function.Tuples
import spock.lang.Ignore
import spock.lang.Specification

import java.util.stream.IntStream

@SpringBootTest
class MapTileServiceImplTests extends Specification {

    @Autowired
    MapTileService mapTileService

    Random random = new Random()

    // FIXME: this test assumes you have a preloaded index with a bunch of data.  on one hand it'd be annoying to create a bunch of data.  on the other, that makes this
    // less useful in the general case.  need to think about data setup for performance tests some more....
    @Ignore("this is not as useful as you'd think for various ES-related caching reasons. but it's maybe the beginning of how we'd want to do some simple performance tests?")
    def "performance testing"() {
        given:
        int zoomLevel = 5
        int totalNumberOfTilesInSingleDimension = (int)(Math.pow(2, zoomLevel))
        int windowSize = 7
        int iterations = 10

        expect:
        Flux<Integer> coordinates = Flux.create(emitter -> {
            int start = random.nextInt(totalNumberOfTilesInSingleDimension - windowSize)
            IntStream.range(start, start+windowSize).forEach(intConsumer -> emitter.next(intConsumer))
            emitter.complete()
        })
        .cast(Integer.class)

        Flux<Integer> xCoordinates = Flux.from(coordinates)
        Flux<Integer> yCoordinates = Flux.from(coordinates)

        Flux<byte[]> tiles = Flux.merge(
                xCoordinates.flatMap(x ->
                        yCoordinates.map(y -> Tuples.of(x, y))
                ).map(tuple ->
                        mapTileService.generateSubTermsTile(new MapTileCoordinates(tuple.getT1(), tuple.getT2(), zoomLevel), "mcc", null)
                )
        )

        long begin = System.currentTimeMillis()
        iterations.times {
            StepVerifier.create(tiles)
                    .expectNextCount(windowSize * windowSize)
                    .verifyComplete()
        }
        long end = System.currentTimeMillis()
        System.out.println("Took ${(end - begin) / 1000} seconds for ${iterations} iterations.")
        System.out.println("Rate is ${(end - begin) / iterations} milliseconds per iteration")

    }
}
