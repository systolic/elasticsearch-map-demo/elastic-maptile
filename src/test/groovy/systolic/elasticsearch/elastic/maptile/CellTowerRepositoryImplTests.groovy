package systolic.elasticsearch.elastic.maptile

import org.apache.http.HttpHost
import org.elasticsearch.ElasticsearchStatusException
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestClient
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.client.indices.CreateIndexRequest
import org.elasticsearch.client.indices.PutMappingRequest
import org.elasticsearch.index.query.QueryBuilder
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.aggregations.Aggregation
import org.elasticsearch.search.aggregations.bucket.geogrid.ParsedGeoTileGrid
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates
import reactor.blockhound.BlockHound
import reactor.core.publisher.Flux
import reactor.core.publisher.Hooks
import reactor.test.StepVerifier
import spock.lang.Specification

import java.time.Duration

// FIXME: this entire test approach is suspicious/wrong.  the method
// to query elasticsearch shouldn't even be public.  however, we're waiting
// for the next release of spring-data-elasticsearch before redoing any of this
// as the newer version makes certain testing options significantly easier
@SpringBootTest
class CellTowerRepositoryImplTests extends Specification {

    @Autowired
    CellTowerRepositoryImpl cellTowerRepository

    @Autowired
    RestHighLevelClient syncClient

    @Autowired
    TestReactiveElasticsearchTemplate elasticsearchTemplate

    private final static String TEST_INDEX_NAME = 'cell-towers-test-index'

    @TestConfiguration
    static class TestConfig {
        @Autowired
        ReactiveElasticsearchClient reactiveElasticsearchClient

        @Bean
        ReactiveElasticsearchTemplate reactiveElasticsearchTemplate() {
            TestReactiveElasticsearchTemplate testReactiveElasticsearchTemplate = new TestReactiveElasticsearchTemplate(reactiveElasticsearchClient)
            testReactiveElasticsearchTemplate.testIndexName = TEST_INDEX_NAME
            return testReactiveElasticsearchTemplate
        }

        @Bean
        RestHighLevelClient restHighLevelClient() {
            return new RestHighLevelClient(
                    RestClient
                            .builder(new HttpHost("localhost", 9200))
                            .setRequestConfigCallback(config -> config
                                    .setConnectTimeout(10000)
                                    .setConnectionRequestTimeout(10000)
                                    .setSocketTimeout(10000)
                            ));
        }
    }

    def setupSpec() {
        Hooks.onOperatorDebug()
        BlockHound.install(new CustomizeBlockHoundForEnvironment())
    }

    def setup() {
        try {
            syncClient.indices().delete(new DeleteIndexRequest(TEST_INDEX_NAME), RequestOptions.DEFAULT)
        } catch (ElasticsearchStatusException e) {
            // ignored
        }
        Map<String, Object> jsonMap = ["properties":
                                       [
                                               "location": ["type": "geo_point"],
                                               "radio": ["type": "keyword"]
                                       ]
        ]
        syncClient.indices().create(new CreateIndexRequest(TEST_INDEX_NAME), RequestOptions.DEFAULT)
        syncClient.indices().putMapping(new PutMappingRequest(TEST_INDEX_NAME).source(jsonMap), RequestOptions.DEFAULT)

        elasticsearchTemplate.saveAll(cellTowers(), IndexCoordinates.of(TEST_INDEX_NAME)).blockLast(Duration.ofMillis(5000))
    }

    def "verify sunny day case contains no blocking calls"() {
        given:
        MapTileCoordinates coordinates = new MapTileCoordinates(543, 819, 11)

        when:
        cellTowerRepository.queryElasticsearch(coordinates, null, null).collectList().block()

        then:
        notThrown(Exception.class)
    }

    def "elastic search query can be issued based on map tile coordinates"() {
        given:
        MapTileCoordinates coordinates = new MapTileCoordinates(67, 102, 8)

        when:
        Flux<Aggregation> aggregationFlux = cellTowerRepository.queryElasticsearch(coordinates, null, null)

        then:
        StepVerifier.create(aggregationFlux)
                .consumeNextWith(aggregation -> {
                    assert aggregation instanceof ParsedGeoTileGrid
                    assert aggregation.getBuckets().size() == 3
                })
                .verifyComplete()
    }

    def "elasticsearch query can be limited with an additional filter"() {
        given:
        MapTileCoordinates coordinates = new MapTileCoordinates(67, 102, 8)

        when:
        Flux<Aggregation> aggregationFlux = cellTowerRepository.queryElasticsearch(
                coordinates,
                null,
                QueryBuilders.termQuery("radio", "GSM"),
        )

        then:
        StepVerifier.create(aggregationFlux)
                .consumeNextWith(aggregation -> {
                    assert aggregation instanceof ParsedGeoTileGrid
                    assert aggregation.getBuckets().size() == 2
                })
                .verifyComplete()
    }

    def "can count all documents"() {
        expect:
        cellTowerRepository.countAll(null).block() == 4
    }

    def "can count subset of documents with bounding box and filter"() {
        given:
        BoundingBox region = new BoundingBox(35.0, -85.0, 33.0, -84.0)
        QueryBuilder additionalFilter = QueryBuilders.termQuery("radio", "GSM")

        expect:
        cellTowerRepository.countInRegion(region, additionalFilter).block() == 2
    }

    private List<CellTower> cellTowers() {
        return [
                cellTower('GSM', 33.7469975, -84.4292546),
                cellTower('CDMA', 33.6530686, -84.4157993),
                cellTower('GSM', 33.7625488, -84.3947816),
                cellTower('GSM', 33.9425942, -83.3781045)
        ]
    }

    private CellTower cellTower() {
        return cellTower('GSM', 33.7469975, -84.4292546)
    }

    private CellTower cellTower(String radio, double lat, double lon) {
        CellTower cellTower = new CellTower()
        cellTower.radio = radio
        cellTower.countryCode = 'US'
        cellTower.mcc = 311
        cellTower.location = new Location(lat, lon)

        return cellTower
    }

}
