package systolic.elasticsearch.elastic.maptile;

import org.elasticsearch.index.query.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class MapTileServiceImpl implements MapTileService {

    @Autowired
    private CellTowerRepository cellTowerRepository;

    @Autowired
    private WifiNetworkRepository wifiNetworkRepository;

    @Value("${elastic.maptile.granularityStep}")
    private int granularityStep;

    @Value("classpath:/test.png")
    private Resource testTileResource;

    @Override
    public Mono<byte[]> generateHeatmapTile(final MapTileCoordinates coordinates, final QueryBuilder additionalFilter) {
        ColorScheme colorScheme = DocCountInterpolatedColorScheme.createBlues(coordinates.getZ(), granularityStep);
        return generateCellTowerTile(coordinates, null, additionalFilter, new HeatmapPngGenerator(colorScheme));
    }

    @Override
    public Mono<byte[]> generateHeatmapTile(final Class<?> entityType, final MapTileCoordinates coordinates, final QueryBuilder additionalFilter) {
        ColorScheme colorScheme = DocCountInterpolatedColorScheme.createBlues(coordinates.getZ(), granularityStep);
        if (CellTower.class.isAssignableFrom(entityType)) {
            return generateCellTowerTile(coordinates, null, additionalFilter, new HeatmapPngGenerator(colorScheme));
        } else if (WifiNetwork.class.isAssignableFrom(entityType)) {
            return generateWifiNetworkTile(coordinates, null, additionalFilter, new HeatmapPngGenerator(colorScheme));
        } else {
            return Mono.error(new IllegalArgumentException("Unsupported class type"));
        }
    }

    @Override
    public Mono<byte[]> generateDebugTile(MapTileCoordinates coordinates, QueryBuilder additionalFilter) {
        return generateCellTowerTile(coordinates, null, additionalFilter, new DebugPngGenerator());
    }

    @Override
    public Mono<byte[]> generateTestTile(final MapTileCoordinates coordinates, final QueryBuilder additionalFilter) {
        return generateCellTowerTile(coordinates, null, additionalFilter, new TestPngGenerator(testTileResource));
    }

    @Override
    public Mono<byte[]> generateSubTermsTile(final MapTileCoordinates coordinates, final String term, final QueryBuilder additionalFilter) {
        ColorScheme colorScheme;
        if (term.equals("radio")) {
            colorScheme = new RadioTypeColorScheme(coordinates.getZ(), granularityStep);
        } else {
            colorScheme = new LargestSubTermColorScheme(coordinates.getZ(), granularityStep);
        }
        return generateCellTowerTile(coordinates, term, additionalFilter, new HeatmapPngGenerator(colorScheme));
    }

    @Override
    public Mono<byte[]> generateSubTermsTile(final Class<?> entityType, final MapTileCoordinates coordinates, final String term, final QueryBuilder additionalFilter) {
        ColorScheme colorScheme;
        if (term.equals("radio")) {
            colorScheme = new RadioTypeColorScheme(coordinates.getZ(), granularityStep);
        } else {
            colorScheme = new LargestSubTermColorScheme(coordinates.getZ(), granularityStep);
        }
        if (CellTower.class.isAssignableFrom(entityType)) {
            return generateCellTowerTile(coordinates, term, additionalFilter, new HeatmapPngGenerator(colorScheme));
        } else if (WifiNetwork.class.isAssignableFrom(entityType)) {
            return generateWifiNetworkTile(coordinates, term, additionalFilter, new HeatmapPngGenerator(colorScheme));
        } else {
            return Mono.error(new IllegalArgumentException("Unsupported class type"));
        }
    }

    private Mono<byte[]> generateCellTowerTile(final MapTileCoordinates coordinates, final String term, final QueryBuilder additionalFilter, PngGenerator pngGenerator) {
        return cellTowerRepository.aggregateForGeographicalCoordinates(coordinates, term, additionalFilter)
                .map(aggregation -> pngGenerator.generatePng(coordinates, aggregation))
                .name("tile-generation")
                .metrics();

    }

    private Mono<byte[]> generateWifiNetworkTile(final MapTileCoordinates coordinates, final String term, final QueryBuilder additionalFilter, PngGenerator pngGenerator) {
        return wifiNetworkRepository.aggregateForGeographicalCoordinates(coordinates, term, additionalFilter)
                .map(aggregation -> pngGenerator.generatePng(coordinates, aggregation))
                .name("tile-generation")
                .metrics();

    }

}
