package systolic.elasticsearch.elastic.maptile;

import java.awt.*;
import java.util.Map;

public class RadioTypeColorScheme implements ColorScheme {

    private Map<String, ColorScheme> colorSchemesByRadioType;

    public RadioTypeColorScheme(int zoomLevel, int granularityStep) {
        colorSchemesByRadioType = Map.of(
                "CDMA", DocCountInterpolatedColorScheme.createReds(zoomLevel, granularityStep),
                "GSM", DocCountInterpolatedColorScheme.createGreens(zoomLevel, granularityStep),
                "LTE", DocCountInterpolatedColorScheme.createYellows(zoomLevel, granularityStep),
                "UMTS", DocCountInterpolatedColorScheme.createBlues(zoomLevel, granularityStep));
    }

    @Override
    public Color getColor(String bucketKey, Long docCount) {
        ColorScheme colorScheme = colorSchemesByRadioType.get(bucketKey);
        if (colorScheme == null) {
            return new Color(255, 255, 255);
        }
        return colorScheme.getColor(bucketKey, docCount);
    }

}
