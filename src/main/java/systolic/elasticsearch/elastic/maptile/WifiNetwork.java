package systolic.elasticsearch.elastic.maptile;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.GeoPointField;

import java.time.Instant;

@Data
@Document(indexName = "wifi-networks", shards = 1, replicas = 0)
public class WifiNetwork {
    @Id
    String id;
    @Field(type = FieldType.Keyword)
    String bssid;
    @Field(type = FieldType.Keyword)
    String manufactor;
    @GeoPointField
    Location location;
    @Field(type = FieldType.Keyword)
    String source;
    Integer measurements;
    Instant lastUpdated;
}
