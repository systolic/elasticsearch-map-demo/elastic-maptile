package systolic.elasticsearch.elastic.maptile;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface WifiNetworkRepository {

    default Flux<WifiNetwork> findNear(double latitude, double longitude, String distance) {
        return findNear(latitude, longitude, distance, null);
    }
    default Flux<WifiNetwork> findNear(double latitude, double longitude, String distance, QueryBuilder additionalFilter) {
        return findNear(latitude, longitude, distance, additionalFilter, -1);
    }
    Flux<WifiNetwork>  findNear(double latitude, double longitude, String distance, QueryBuilder additionalFilter, Integer maximumNumberOfResults);

    default Mono<Long> countInRegion(BoundingBox boundingBox) { return countInRegion(boundingBox, null); }
    Mono<Long> countInRegion(BoundingBox boundingBox, QueryBuilder additionalFilter);

    default Mono<Long> countAll() { return countAll(null); }
    Mono<Long> countAll(QueryBuilder additionalFilter);

    Mono<MultiBucketsAggregation> aggregateForGeographicalCoordinates(MapTileCoordinates coordinates, String termFieldSubAggregation, QueryBuilder additionalFilter);

}
