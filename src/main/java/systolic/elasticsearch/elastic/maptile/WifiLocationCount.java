package systolic.elasticsearch.elastic.maptile;

import lombok.Data;
import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;

@Data
public class WifiLocationCount {
    Location location;
    Long count;

    public WifiLocationCount(MultiBucketsAggregation.Bucket bucket) {
        this(new BoundingBox(bucket.getKeyAsString()), bucket.getDocCount());
    }

    public WifiLocationCount(BoundingBox boundingBox, Long count) {
        this.location = new Location();
        this.location.lat = boundingBox.getCenterLatitude();
        this.location.lon = boundingBox.getCenterLongitude();

        this.count = count;
    }

}
