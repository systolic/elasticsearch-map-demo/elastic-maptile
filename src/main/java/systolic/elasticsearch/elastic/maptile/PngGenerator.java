package systolic.elasticsearch.elastic.maptile;

import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;

public interface PngGenerator {
    byte[] generatePng(MapTileCoordinates tileCoordinates, MultiBucketsAggregation aggregation);
}
