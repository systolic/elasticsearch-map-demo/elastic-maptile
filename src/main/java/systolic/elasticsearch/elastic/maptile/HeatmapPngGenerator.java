package systolic.elasticsearch.elastic.maptile;

import lombok.Value;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;
import org.elasticsearch.search.aggregations.bucket.geogrid.GeoGrid;
import org.elasticsearch.search.aggregations.bucket.geogrid.ParsedGeoGrid;
import org.elasticsearch.search.aggregations.bucket.geogrid.ParsedGeoTileGrid;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import reactor.core.Exceptions;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class HeatmapPngGenerator implements PngGenerator {

    private final ColorScheme colorScheme;

    public HeatmapPngGenerator(ColorScheme colorScheme) { this.colorScheme = colorScheme; }

    @Override
    public byte[] generatePng(MapTileCoordinates tileCoordinates, MultiBucketsAggregation aggregation) {
        Map<String, Long> maximumCounts = new HashMap<>();

        BoundingBox tileBoundingBox = SphericalMercatorProjection.wgs84ToSphericalMercator(new BoundingBox(tileCoordinates));
        Scale scale = new Scale(tileBoundingBox, 256);

        BufferedImage img = new BufferedImage(256, 256, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = img.createGraphics();
        g2d.setComposite(AlphaComposite.Clear);
        g2d.fillRect(0, 0, 256, 256);
        g2d.setComposite(AlphaComposite.SrcOver);

        if (aggregation instanceof ParsedTerms) {
            ParsedTerms termsAggregation = (ParsedTerms) aggregation;
            for (Terms.Bucket bucket : termsAggregation.getBuckets()) {
                String key = bucket.getKeyAsString();
                ParsedGeoTileGrid geoTileGrid = (ParsedGeoTileGrid) bucket.getAggregations().asList().get(0);

                for (GeoGrid.Bucket geoBucket : geoTileGrid.getBuckets()) {
                    String zxy = geoBucket.getKeyAsString();
                    Long previousMaximum = maximumCounts.getOrDefault(zxy, 0L);
                    if (geoBucket.getDocCount() > previousMaximum) {
                        maximumCounts.put(zxy, geoBucket.getDocCount());

                        writeToGraphics(g2d, key, zxy, geoBucket.getDocCount(), tileBoundingBox, scale, colorScheme);
                    }
                }
            }

        } else if (aggregation instanceof ParsedGeoGrid) {
            ParsedGeoGrid parsedGeoGrid = (ParsedGeoGrid) aggregation;
            for (GeoGrid.Bucket bucket : parsedGeoGrid.getBuckets()) {
                writeToGraphics(g2d, bucket, tileBoundingBox, scale, colorScheme);
            }
        } else {
            throw new IllegalArgumentException("Unknown aggregation type for PNG generation");
        }
        return imageToByteArray(img);

    }

    private byte[] imageToByteArray(BufferedImage image) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(byteArrayOutputStream);

        try {
            ImageIO.write(image, "PNG", bufferedOutputStream);
        } catch (IOException e) {
            throw Exceptions.propagate(e);
        }

        return byteArrayOutputStream.toByteArray();
    }

    private void writeToGraphics(Graphics2D g2d, String bucketKey, String zxy, Long bucketCount, BoundingBox tileBoundingBox, Scale scale, ColorScheme colorScheme) {
        BoundingBox bucketBoundingBox = SphericalMercatorProjection.wgs84ToSphericalMercator(new BoundingBox(new MapTileCoordinates(zxy)));

        OffsetBox offsetBox = new OffsetBox(bucketBoundingBox, tileBoundingBox);

        g2d.setColor(colorScheme.getColor(bucketKey, bucketCount));
        g2d.fillRect(scale.scaleX(offsetBox), scale.scaleY(offsetBox), scale.scaleWidth(offsetBox), scale.scaleHeight(offsetBox));
    }


    private void writeToGraphics(Graphics2D g2d, GeoGrid.Bucket bucket, BoundingBox tileBoundingBox, Scale scale, ColorScheme colorScheme) {
        BoundingBox bucketBoundingBox = SphericalMercatorProjection.wgs84ToSphericalMercator(new BoundingBox(bucket.getKeyAsString()));

        OffsetBox offsetBox = new OffsetBox(bucketBoundingBox, tileBoundingBox);

        g2d.setColor(colorScheme.getColor(bucket.getKeyAsString(), bucket.getDocCount()));
        g2d.fillRect(scale.scaleX(offsetBox), scale.scaleY(offsetBox), scale.scaleWidth(offsetBox), scale.scaleHeight(offsetBox));
    }

    @Value
    private static class OffsetBox {
        double offsetX;
        double offsetY;
        double width;
        double height;

        public OffsetBox(BoundingBox bucketBoundingBox, BoundingBox tileBoundingBox) {
            offsetX = bucketBoundingBox.getWest() - tileBoundingBox.getWest();
            offsetY = bucketBoundingBox.getNorth() - tileBoundingBox.getNorth();
            width = Math.abs(bucketBoundingBox.getWest() - bucketBoundingBox.getEast());
            height = Math.abs(bucketBoundingBox.getSouth() - bucketBoundingBox.getNorth());
        }
    }

    @Value
    private static class Scale {
        double xScale;
        double yScale;

        public Scale(BoundingBox tileBoundingBox, int imageSize) {
            xScale = Math.abs(tileBoundingBox.getWest() - tileBoundingBox.getEast()) / imageSize;
            yScale = Math.abs(tileBoundingBox.getSouth() - tileBoundingBox.getNorth()) / imageSize;
        }

        public int scaleX(OffsetBox offsetBox) {
            return Math.abs((int) Math.round(offsetBox.getOffsetX() / xScale));
        }

        public int scaleY(OffsetBox offsetBox) {
            return Math.abs((int) Math.round(offsetBox.getOffsetY() / yScale));
        }

        public int scaleWidth(OffsetBox offsetBox) {
            return Math.abs((int) Math.round(offsetBox.getWidth() / xScale));
        }

        public int scaleHeight(OffsetBox offsetBox) {
            return Math.abs((int) Math.round(offsetBox.getHeight() / yScale));
        }
    }
}
