package systolic.elasticsearch.elastic.maptile;

import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;

import java.awt.*;
import java.util.List;

public class LargestSubTermColorScheme implements ColorScheme {

    private List<ColorScheme> availableHues;

    public LargestSubTermColorScheme(int zoomLevel, int granularityStep) {
        availableHues = List.of(
                DocCountInterpolatedColorScheme.createBlues(zoomLevel, granularityStep),
                DocCountInterpolatedColorScheme.createOranges(zoomLevel, granularityStep),
                DocCountInterpolatedColorScheme.createGreens(zoomLevel, granularityStep),
                DocCountInterpolatedColorScheme.createReds(zoomLevel, granularityStep),
                DocCountInterpolatedColorScheme.createPurples(zoomLevel, granularityStep),
                DocCountInterpolatedColorScheme.createGreys(zoomLevel, granularityStep),
                DocCountInterpolatedColorScheme.createPinks(zoomLevel, granularityStep),
                DocCountInterpolatedColorScheme.createYellows(zoomLevel, granularityStep),
                DocCountInterpolatedColorScheme.createTeals(zoomLevel, granularityStep));
    }

    @Override
    public Color getColor(String bucketKey, Long docCount) {
        Long key = 0L;
        try {
            key = Long.parseLong(bucketKey);
        } catch (NumberFormatException e) {
            key = (long) bucketKey.hashCode();
        }

        return availableHues.get((int) (key % availableHues.size())).getColor(bucketKey, docCount);
    }


    private Long naturalKeyForBucket(Terms.Bucket bucket) {
        try {
            return bucket.getKeyAsNumber().longValue();
        } catch (NumberFormatException e) {
            return (long) bucket.getKeyAsString().hashCode();
        }
    }

}
