package systolic.elasticsearch.elastic.maptile;

import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;

import java.awt.*;

public class SimpleColorScheme implements ColorScheme {

    @Override
    public Color getColor(String key, Long docCount) { return Color.BLUE; }
}
