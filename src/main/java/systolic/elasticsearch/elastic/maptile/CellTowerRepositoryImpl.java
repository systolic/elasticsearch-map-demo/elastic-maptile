package systolic.elasticsearch.elastic.maptile;

import io.netty.handler.timeout.TimeoutException;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient;
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.time.Duration;

@Repository
public class CellTowerRepositoryImpl implements CellTowerRepository {

    @Autowired
    private ReactiveElasticsearchClient elasticsearchClient;

    @Autowired
    private ReactiveElasticsearchTemplate elasticsearchTemplate;

    @Value("${elastic.maptile.granularityStep}")
    int granularityStep;

    @Override
    public Flux<CellTower> findNear(double latitude, double longitude, String distance, QueryBuilder additionalFilter, Integer maximumNumberOfResults) {
        BoolQueryBuilder searchQuery = QueryBuilders.boolQuery()
                .filter(geoDistanceQuery(latitude, longitude, distance));
        if (additionalFilter != null) {
            searchQuery.filter(additionalFilter);
        }
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder().withQuery(searchQuery);
        if (maximumNumberOfResults != null && maximumNumberOfResults > 0) {
            queryBuilder.withPageable(PageRequest.of(0, maximumNumberOfResults)); // FIXME: use query.setMaxResults() in future when available
        }

        return elasticsearchTemplate.search(queryBuilder.build(), CellTower.class)
                .map(SearchHit::getContent)
                .retryWhen(Retry.backoff(3, Duration.ofMillis(250))
                        .filter(t -> t instanceof TimeoutException));
    }

    @Override
    public Mono<Long> countInRegion(BoundingBox boundingBox, QueryBuilder additionalFilter) {
        return queryElasticsearch(createQuery(boundingBox, additionalFilter))
                .name("elastic-counts")
                .metrics();
    }

    @Override
    public Mono<Long> countAll(QueryBuilder additionalFilter) {
        return queryElasticsearch(createAllQuery(additionalFilter))
                .name("elastic-counts")
                .metrics();
    }

    @Override
    public Mono<MultiBucketsAggregation> aggregateForGeographicalCoordinates(MapTileCoordinates coordinates, String termFieldSubAggregation, QueryBuilder additionalFilter) {
        return queryElasticsearch(coordinates, termFieldSubAggregation, additionalFilter)
                .name("elastic-aggregation")
                .metrics()
                .cast(MultiBucketsAggregation.class)
                .single();
    }

    Mono<Long> queryElasticsearch(QueryBuilder query) {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                .query(query);
        SearchRequest countRequest = new SearchRequest(cellTowerIndexName())
                .source(searchSourceBuilder);
        return elasticsearchClient.count(countRequest);
    }

    Flux<Aggregation> queryElasticsearch(MapTileCoordinates coordinates, String termFieldSubAggregation, QueryBuilder additionalFilter) {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(createQuery(coordinates, additionalFilter));
        searchSourceBuilder.size(0);

        AggregationBuilder aggregationBuilder = null;
        if (termFieldSubAggregation != null) {
            aggregationBuilder = AggregationBuilders
                    .terms(termFieldSubAggregation)
                    .field(termFieldSubAggregation)
                    .size(40) // FIXME: there are only a handful of tiles at certain zoom levels where this is required.  for the most part, the standard 10 is fine. should we try to do something about that?
                    .subAggregation(AggregationBuilders.geotileGrid("geotile")
                            .field("location").precision(calculatePrecision(coordinates))
                            .size(70000)
                    );

        } else {
            aggregationBuilder = AggregationBuilders.geotileGrid("agg")
                    .field("location").precision(calculatePrecision(coordinates))
                    .size(70000);
        }

        searchSourceBuilder.aggregation(aggregationBuilder);

        SearchRequest searchRequest = new SearchRequest(cellTowerIndexName());

        searchRequest.source(searchSourceBuilder);
        return elasticsearchClient.aggregate(searchRequest)
                .retryWhen(Retry.backoff(3, Duration.ofMillis(250))
                        .filter(t -> t instanceof TimeoutException));
    }

    private String cellTowerIndexName() {
        return elasticsearchTemplate.getIndexCoordinatesFor(CellTower.class).getIndexName();
    }

    private int calculatePrecision(MapTileCoordinates coordinates) {
        return Math.min(coordinates.getZ() + granularityStep, 29);
    }

    private GeoDistanceQueryBuilder geoDistanceQuery(double latitude, double longitude, String distance) {
        return QueryBuilders.geoDistanceQuery("location").point(latitude, longitude).distance(distance);
    }

    private QueryBuilder createQuery(MapTileCoordinates coordinates, QueryBuilder additionalFilter) {
        BoolQueryBuilder searchQuery = QueryBuilders.boolQuery()
                .filter(geoQuery(coordinates));
        if (additionalFilter != null) {
            searchQuery.filter(additionalFilter);
        }
        return searchQuery;
    }

    private QueryBuilder createQuery(BoundingBox boundingBox, QueryBuilder additionalFilter) {
        BoolQueryBuilder searchQuery = QueryBuilders.boolQuery()
                .filter(geoQuery(boundingBox));
        if (additionalFilter != null) {
            searchQuery.filter(additionalFilter);
        }
        return searchQuery;
    }

    private QueryBuilder createAllQuery(QueryBuilder additionalFilter) {
        if (additionalFilter != null) {
            return additionalFilter;
        } else {
            return QueryBuilders.matchAllQuery();
        }
    }

    private GeoBoundingBoxQueryBuilder geoQuery(MapTileCoordinates coordinates) {
        BoundingBox boundingBox = new BoundingBox(coordinates);
        return QueryBuilders.geoBoundingBoxQuery("location").setCorners(boundingBox.getNorth(), boundingBox.getWest(), boundingBox.getSouth(), boundingBox.getEast());
    }

    private GeoBoundingBoxQueryBuilder geoQuery(BoundingBox boundingBox) {
        return QueryBuilders.geoBoundingBoxQuery("location").setCorners(boundingBox.getNorth(), boundingBox.getWest(), boundingBox.getSouth(), boundingBox.getEast());
    }
}
