package systolic.elasticsearch.elastic.maptile;

import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;

import java.awt.*;
import java.util.List;

public class DocCountInterpolatedColorScheme implements ColorScheme {

    private long maxDocumentCount;
    private RgbBasisSplineInterpolator interpolator;

    public DocCountInterpolatedColorScheme(int zoomLevel, int granularityStep, List<String> colors) {
        maxDocumentCount = getMaxDocCountForZoomLevel(zoomLevel, granularityStep);
        interpolator = new RgbBasisSplineInterpolator(colors);
    }

    @Override
    public Color getColor(String bucketKey, Long docCount) {
        double intensityScale = Math.log(docCount) / Math.log(maxDocumentCount);
        return interpolator.interpolate(intensityScale);
    }

    public static DocCountInterpolatedColorScheme createBlues(int zoomLevel, int granularityStep) {
        return new DocCountInterpolatedColorScheme(zoomLevel, granularityStep, List.of(
                "08306b", "08519c", "2171b5", "4292c6", "6baed6", "9ecae1", "c6dbef", "deebf7", "f7fbff"
        ));
    }

    public static DocCountInterpolatedColorScheme createGreens(int zoomLevel, int granularityStep) {
        return new DocCountInterpolatedColorScheme(zoomLevel, granularityStep, List.of(
                "00441b", "006d2c", "238b45", "41ab5d", "74c476", "a1d99b", "c7e9c0", "e5f5e0", "f7fcf5"
        ));
    }

    public static DocCountInterpolatedColorScheme createGreys(int zoomLevel, int granularityStep) {
        return new DocCountInterpolatedColorScheme(zoomLevel, granularityStep, List.of(
                "000000", "252525", "525252", "737373", "969696", "bdbdbd", "d9d9d9", "f0f0f0", "ffffff"
        ));
    }

    public static DocCountInterpolatedColorScheme createOranges(int zoomLevel, int granularityStep) {
        return new DocCountInterpolatedColorScheme(zoomLevel, granularityStep, List.of(
                "7f2704", "a63603", "d94801", "f16913", "fd8d3c", "fdae6b", "fdd0a2", "fee6ce", "fff5eb"
        ));
    }

    public static DocCountInterpolatedColorScheme createPurples(int zoomLevel, int granularityStep) {
        return new DocCountInterpolatedColorScheme(zoomLevel, granularityStep, List.of(
                "3f007d", "54278f", "6a51a3", "807dba", "9e9ac8", "bcbddc", "dadaeb", "efedf5", "fcfbfd"
        ));
    }

    public static DocCountInterpolatedColorScheme createReds(int zoomLevel, int granularityStep) {
        return new DocCountInterpolatedColorScheme(zoomLevel, granularityStep, List.of(
                "67000d", "a50f15", "cb181d", "ef3b2c", "fb6a4a", "fc9272", "fcbba1", "fee0d2", "fff5f0"
        ));
    }

    public static DocCountInterpolatedColorScheme createPinks(int zoomLevel, int granularityStep) {
        return new DocCountInterpolatedColorScheme(zoomLevel, granularityStep, List.of(
                "5f086b", "7d089c", "8f21b5", "9b42c6", "b16bd6", "c89ee1", "e6c6ef", "f1def7", "fdf7ff"
        ));
    }

    public static DocCountInterpolatedColorScheme createYellows(int zoomLevel, int granularityStep) {
        return new DocCountInterpolatedColorScheme(zoomLevel, granularityStep, List.of(
                "596b08", "8f9c08", "afb521", "c6c342", "d6d16b", "e1dc9e", "ecefc6", "f5f7de", "fefff7"
        ));
    }

    public static DocCountInterpolatedColorScheme createTeals(int zoomLevel, int granularityStep) {
        return new DocCountInterpolatedColorScheme(zoomLevel, granularityStep, List.of(
                "086b5d", "089c7a", "21b58c", "42c699", "6bd6b0", "9ee1c7", "c6efe5", "def7f1", "f7fffd"
        ));
    }

    private static long getMaxDocCountForZoomLevel(int zoomLevel, int granularityStep) {
        int bucketZoomLevel = zoomLevel + granularityStep;

        switch (bucketZoomLevel) {
            case 7:
                return 590000;

            case 8:
                return 480000;

            case 9:
                return 350000;

            case 10:
                return 220000;

            case 11:
                return 158000;

            case 12:
                return 66000;

            case 13:
                return 25000;

            case 14:
                return 9700;

            case 15:
                return 4900;

            case 16:
                return 2000;

            case 17:
                return 800;

            default:
                return 720;
        }
    }

}
